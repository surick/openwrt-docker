#!/bin/sh
if [ -f openwrt-tree.tar.gz ]; then
	echo "CI Build detected.. extracting openwrt tree"
	tar -zxf openwrt-tree.tar.gz
fi
make -C openwrt -j$(nproc)
ROOT_TARGZ=$(find openwrt/bin -name "*rootfs.tar.gz")

cp "${ROOT_TARGZ}" container/openwrt-root.tar.gz
