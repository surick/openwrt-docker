# OpenWrt in Docker

This repository builds a OpenWrt router that works inside Docker (and possibly other container solutions).

There is a small wrapper script for init that removes the pre-assigned IP address that Docker sets. If you know how
to prevent Docker from setting the IP addresses in the first place, let me know!

You can modify the build scripts here for your own needs, such as adding custom feeds and/or packages. By default, useful
packages such as luci are installed.

## Prebuilt images
Prebuilt images for x86-64 and ARM64 (via Gitlab-CI) are available on the [registry](https://gitlab.com/mcbridematt/openwrt-docker/container_registry) page:
```
# x86-64
docker pull registry.gitlab.com/mcbridematt/openwrt-docker/x64
# arm64
docker pull registry.gitlab.com/mcbridematt/openwrt-docker/arm64
```

## Usage - as a containerized router
For this example, we will use the macvlan driver in Docker to bind two "physical" ethernet interfaces to OpenWrt.

First, create the networks in Docker:
```
#!/bin/sh
ip link set eth1 up
ip link set eth2 up
docker network create -d macvlan \
  -o parent=eth1 network_01
docker network create -d macvlan \
  -o parent=eth2 network_02
```
**Important:** Note that networks will be bound to the container in the order they were created in Docker,
so in the example above, eth1 will always be 'eth0' inside the container.

Then, create the container:
```
#!/bin/sh
set -e

CONTAINER_NAME=openwrt-container
# NET_ADMIN capabilities are required so the container can set interface IP addresses, routes etc.
# inside it's own network namespace.
docker create --name "${CONTAINER_NAME}" --cap-add NET_ADMIN --network network_01 -i -t openwrt-container
docker network connect network_02 "${CONTAINER_NAME}"
docker start openwrt-container

# If you wish to explore the containers network namespace from the host (see tutorial in Acknowlegements):
pid="$(docker inspect -f '{{.State.Pid}}' ${CONTAINER_NAME})"
ln -sf /proc/${pid}/ns/net "/var/run/netns/${CONTAINER_NAME}"
```
## Acknowledgements
* [Container utility feed for OpenWrt](https://gitlab.com/mcbridematt/openwrt-container-feed) - sister repository, this provides Docker host capabilities on OpenWrt
* [mimka's openwrt-lxd](https://github.com/mikma/lxd-openwrt) for running OpenWrt in LXD (another container technology)
* [Docker network namespace tutorial](https://platform9.com/blog/container-namespaces-deep-dive-container-networking/)
